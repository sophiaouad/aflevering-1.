$Title Problem 3.
Sets
v "ships" /v1,v2,v3,v4,v5/
r "routes" /Asia, ChinaPacific/
p "ports" /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
*We  will make two subsets of P*
H1(p) "port pairs 1" /Singapore,Osaka/
H2(p) "port pairs 2" /Incheon, Victoria/
;

*We will create the parameters we got from the text and table 3 and 4*
Parameters

F(v) "Fixed cost while using ship v"
/v1 65
 v2 60
 v3 92
 v4 100
 v5 110/

G(v) "Number of days the ship can sail"
/v1 300
 v2 250
 v3 350
 v4 330
 v5 300/
 
D(p) "Number of times they have to visit the ports every year"
/Singapore  15
 Incheon    18
 Shanghai   32
 Sydney     32
 Gladstone  45
 Dalian     32
 Osaka      15
 Victoria   18/
 ;
 
Table
C(v,r) "The cost for ship v sailing route r"
    Asia    ChinaPacific
v1  1.41    1.9 
v2  3.0     1.5
v3  0.4     0.8
v4  0.5     0.7 
v5  0.7     0.8
;

Table
T(v,r) "Number of days needed to complete one time route r"
    Asia    ChinaPacific
v1  14.4    21.2
v2  13.0    20.7
v3  14.4    20.6
v4  13.0    19.2
v5  12.0    20.1
;

Table
A(p,r) "Indicator of wheather route r sails through port p"
             Asia    ChinaPacific
Singapore      1         0
Incheon        1         0
Shanghai       1         1
Sydney         0         1
Gladstone      0         1
Dalian         1         1
Osaka          1         0
Victoria       0         1
;

*We are given some scalars from the problem text*
Scalar
K "The number of how many ports the SuperSea has to service" /5/
;
*We will define our variables for our problem* 

Variables
z "Objectfunction which describes the total yearly cost"
x(v) "1 if ship v sails and 0 otherwise"
y(p) "1 if port p is visited and 0 otherwise"
w(v,r) "How many times ship v sails route r"
;

Binary variables
x(v)
y(p)
;

Integer variables
w(v,r)
;

*We want to minmize our cost - so we will make a model*
Equations
Obj "Yearly cost"
Con1 "How many ports the company has to visit"
Con2 "First port pair - either Singapore or Osaka"
Con3 "Second port pair - either Incheon or Victoria "
Con4(p) "If we visit port p we have to do it at least D(p) times"
Con5(v) "Makes sure that our ship v maximum sails G(v) in a year"
;



*We want to minimize the cost. The objective function consists of the sum of cost using ships*
* times if the ship sails. The we have the sum of the ship sailing a routee times how many*
* times the ship sails the route.*
Obj.. z=e= sum(v,F(v)*x(v)) + sum(v,sum(r, C(v,r)*w(v,r)));

*By the problem text we know that the company has to visit the ports at least 5 times*
Con1.. K =l= sum(p,y(p));

*We will make two constraints that secure we visit at most one of them*
Con2.. sum(H1(p), y(p)) =l= 1;
Con3.. sum(H2(p), y(p)) =l= 1;

*If we use ship v to sail route r and visit port p, then we have to visit port p*
*at least D(p) times. Be aware of, if port p is visited then it will be equal to 1*
*and 0 otherwise.*
Con4(p)..  sum(r,sum (v, A(p,r)*w(v,r))) =g= y(p)*D(p) ;

*G(v) is the number of how many days a ship v maximum can sail in a year, it has to be *
* bigger or equal to how many times the ships v sails route r times the number of days it*
*takes to sail the route*
Con5(v).. sum(r,T(v,r)*w(v,r)) =l= G(v)*x(v);


*We will minimize the cost, z.
MODEL Problem3 /all/;
SOLVE Problem3 USING mip MINIMIZING z;

DISPLAY z.l, x.l, y.l, w.l;

