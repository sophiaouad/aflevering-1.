$Title Problem 2.
Sets
K "keys" /k1*k30/
i "nodes" /n1*n8/;

Alias(i,j);
*We are given a amounts of constant which we will set as scalars*
Scalar
mk "Each key k occupies a space of mk" /186/
Mi "The maximum memory limit" /1000/
q "The minimum number of keys a paur of nodes should share to have direct connection" /3/
T The number of times a key should be used" /2/
;


*We will make our variables such our model can be done correctly*
Variables
z "The number of direct connections"
;

Binary variables
x(k,i) "1 if key is assigned to node i, 0 otherwise "
d(i,j) "If 1 node i and j are directly connected, 0 otherwise."
y(i,j,k) "1 if node i and j shares key K, 0 otherwise."

*We will make some constraints which will ensure that we maximizes the number of total direct connections*
Equations
Obj "The objective function - total amount of direct connections"
Con1 (i) "The maximum memory limit"
Con2 (k) "The same key can be used to maximum T nodes"
Con3 (i,j)
Con4 (i,j,k)
Con5 (i,j,k)
;
*We want to maximize the number of direct connection*
Obj..z=e= sum(i,sum(j$(ord(i)<> ord(j) and ord(i)>ord(j)), d(i,j)));

*Keys associated to a given node we must not exceed the memory limit Mi of the node*
Con1(i).. sum(k, mk*x(k,i))  =l=Mi;

*Each key must be used at most T times*
Con2(k).. sum(i, x(k,i))  =l=T;

*Delta will alwways be forced to be 1 because it is maxemazation problem*
Con3(i,j).. sum(k, y(i,j,k)) =g= q*d(i,j);

*We will now make a constraint which will force the definiotion of y(i,j,k) to be 1*
*if node j and i has the same key k, and 0 otherwise.*
Con4(i,j,k).. y(i,j,k)+1 =g= x(k,i)+x(k,j)$(ord(i) <> ord(j) and ord (i)>ord(j));
Con5(i,j,k).. 2*y(i,j,k) =l= x(k,i)+x(k,j)$(ord(i) <> ord(j) and ord (i)>ord(j));

Model problem2 /all/;
Solve problem2 USING mip MAXIMIZING z;

Display z.l, x.l, y.l, d.l;